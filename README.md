
代码自动生成工具
一、简介
1.1 概述

CodeGenerator是一个轻量的代码生成工具，可以在常规的项目开发初始阶段生成model、dao、Mapper、
mapper（mybatis）、service、controller，项目思路来源于mybatis－generator，不过代码更加简洁易控制

1.2 特性
  1.2.1、代码运行既可以下载源码运行AppTest测试用例，也可以使用maven插件的方式运行（推荐使用）
  1.2.2、代码模块可以自定义生成，比如只需要model、dao，Mapper、mapper的代码，可以在配置文件配置，生成目录也可以配置，规则是约定大于配置
  1.2.3、可以修改自定义的模版样式

1.3 使用方式
  1.3.1、首页check源码在本地，如果需要本地测试用例运行，请看AppTest


  public class AppTest {

    private static ApplicationContext context;

    private static GeneratorFactoryImpl generatorFactory;

    @BeforeClass
    public static void beforeClass(){
        try {
            context = new ClassPathXmlApplicationContext("classpath:spring-generator.xml");
            generatorFactory=(GeneratorFactoryImpl)context.getBean("generatorFactory");
        } catch (BeansException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void doAfter(){
        if(context !=null && context instanceof ClassPathXmlApplicationContext){
            ((ClassPathXmlApplicationContext) context).close();
        }
    }

    @Test
    public void codeGeneratorTest() {
        generatorFactory.defaultGeneratorStarter();
    }
}

1.3.2、运行之前需要修改一下spring-generator.xml，配置每个模块的生成器


<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd">

    <!--model模块-->
    <bean id="modelPackageConfigTypes" class="com.oneplus.mybatis.config.PackageConfigTypes">
        <constructor-arg name="type" value="MODEL"/>
        <constructor-arg name="packageConfigTypeSet">
            <set>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <!--model模块文件路径-->
                    <property name="targetDir" value="/dto"/>
                    <!--model模块文件后缀-->
                    <property name="fileNameSuffix" value="{domainSuffix}.java"/>
                    <!--model模块文件使用的模版-->
                    <property name="template" value="domain_dto.vm"/>
                </bean>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/dto/qdto"/>
                    <property name="fileNameSuffix" value="{queryDomainSuffix}.java"/>
                    <property name="template" value="domain_dto_qdto.vm"/>
                </bean>
            </set>
        </constructor-arg>
    </bean>
    <!--sqlMapper模块-->
    <bean id="mapperPackageConfigTypes" class="com.oneplus.mybatis.config.PackageConfigTypes">
        <constructor-arg name="type" value="MAPPER"/>
        <constructor-arg name="packageConfigTypeSet">
            <set>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/dao/mapper"/>
                    <property name="fileNameSuffix" value="Mapper.xml"/>
                    <property name="template" value="dao_sqlMapper.vm"/>
                </bean>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/dao/mapper"/>
                    <property name="fileNameSuffix" value="Mapper.java"/>
                    <property name="template" value="dao_mapper.vm"/>
                </bean>
            </set>
        </constructor-arg>
    </bean>
    <!--Mapper模块-->
    <bean id="mapperConfigPackageConfigTypes" class="com.oneplus.mybatis.config.PackageConfigTypes">
        <constructor-arg name="type" value="MAPPER_CONFIG"/>
        <constructor-arg name="packageConfigTypeSet">
            <set>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/dao"/>
                    <property name="fileNameSuffix" value="mybatis-config.xml"/>
                    <property name="template" value="mybatis-config.vm"/>
                </bean>
            </set>
        </constructor-arg>
    </bean>
    <!--Result模块-->
    <bean id="resultPackageConfigTypes" class="com.oneplus.mybatis.config.PackageConfigTypes">
        <constructor-arg name="type" value="RESULT"/>
        <constructor-arg name="packageConfigTypeSet">
            <set>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/service/module"/>
                    <property name="fileNameSuffix" value="Result.java"/>
                    <property name="template" value="result.vm"/>
                </bean>
            </set>
        </constructor-arg>
    </bean>
    <!--Service模块-->
    <bean id="servicePackageConfigTypes" class="com.oneplus.mybatis.config.PackageConfigTypes">
        <constructor-arg name="type" value="SERVICE"/>
        <constructor-arg name="packageConfigTypeSet">
            <set>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/service"/>
                    <property name="fileNameSuffix" value="Service.java"/>
                    <property name="template" value="service.vm"/>
                </bean>
                <bean class="com.oneplus.mybatis.config.PackageConfigType">
                    <property name="targetDir" value="/service/impl"/>
                    <property name="fileNameSuffix" value="ServiceImpl.java"/>
                    <property name="template" value="service_impl.vm"/>
                </bean>
            </set>
        </constructor-arg>
    </bean>
    <!--Model模块代码生成器-->
    <bean id="modelGenerator" class="com.oneplus.mybatis.generator.impl.ModelGeneratorImpl">
        <property name="packageConfigTypes" ref="modelPackageConfigTypes"/>
    </bean>
    <!--mapper模块代码生成器-->
    <bean id="mapperGenerator" class="com.oneplus.mybatis.generator.impl.MapperGeneratorImpl">
        <property name="packageConfigTypes" ref="mapperPackageConfigTypes"/>
    </bean>
    <!--sqlMapperConfig模块代码生成器-->
    <bean id="mapperConfigGenerator" class="com.oneplus.mybatis.generator.impl.MapperConfigGeneratorImpl">
        <property name="packageConfigTypes" ref="mapperConfigPackageConfigTypes"/>
    </bean>
    <bean id="resultGenerator" class="com.oneplus.mybatis.generator.impl.ResultGeneratorImpl">
        <property name="packageConfigTypes" ref="resultPackageConfigTypes"/>
    </bean>
    <bean id="serviceGenerator" class="com.oneplus.mybatis.generator.impl.ServiceGeneratorImpl">
        <property name="packageConfigTypes" ref="servicePackageConfigTypes"/>
    </bean>
    <!--代码生成工厂-->
    <bean id="generatorFactory" class="com.oneplus.mybatis.generator.base.GeneratorFactoryImpl">
        <property name="generatorSet">
            <set>
                <bean parent="modelGenerator"/>
                <bean parent="mapperGenerator"/>
                <bean parent="mapperConfigGenerator"/>
                <bean parent="resultGenerator"/>
                <bean parent="serviceGenerator"/>
            </set>
        </property>
    </bean>
</beans>

2、运行之前需要修改一下config-generator.properties，配置数据源等信息

##mysql连接配置
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/shiro
jdbc.username=root
jdbc.password=root

generator.authorName=HuJiFang

##是否生成注释
generator.annotation=false

##生成代码位置
generator.location=src

##文件包名称
generator.project.name=main

##生成那些层
generator.layers=mapper,mapperConfig,model,service,result

##包名称
generator.basePackage=com.oneplus.code.temp

##表名称，多个用逗号分隔(,)
generator.tables=sys_privilege_menu

##过滤掉代码表的前缀
generator.table.prefix=sys_

##浮点型转化为：BigDecimal，否则转化为：Double
generator.precision=high

##生成Service domain后缀名称
generator.domain.suffix=DTO

#生成Service query domain后缀名称
generator.queryDomain.suffix=QueryDTO

1.3.3 maven插件使用方式
在pom文件中定义插件


<plugin>
    <groupId>com.oneplus.maven.plugins</groupId>
    <artifactId>code-generator</artifactId>
    <version>${codeGenerator.version}</version>
</plugin>			    


1.4 代码生成略览



<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.hujifang.core.dao.mapper.PrivilegeMenuMapper">

	<resultMap id="privilegeMenuMapper" type="PrivilegeMenuDto">
		<result property="id" column="id"/>
		<result property="menuDesc" column="menu_desc"/>
		<result property="menuName" column="menu_name"/>
		<result property="menuAppId" column="menu_app_id"/>
		<result property="menuCreateTime" column="menu_create_time"/>
		<result property="menuCreatePid" column="menu_create_pid"/>
		<result property="menuCreatePname" column="menu_create_pname"/>
		<result property="menuModifyTime" column="menu_modify_time"/>
		<result property="menuModifyPid" column="menu_modify_pid"/>
		<result property="menuModifyPname" column="menu_modify_pname"/>
		<result property="menuCode" column="menu_code"/>
		<result property="menuStatus" column="menu_status"/>
	</resultMap>

	<sql id="_table">
		sys_privilege_menu
	</sql>

	<sql id="_fields">
		id,menu_desc,menu_name,menu_app_id,menu_create_time,menu_create_pid,menu_create_pname,menu_modify_time,menu_modify_pid,menu_modify_pname,menu_code,menu_status
	</sql>

	<sql id="_condition">
		<trim prefix="WHERE" prefixOverrides="AND|OR">
			<if test="id!=null">
				AND id = #{id}
			</if>
			<if test="menuDesc!=null and ''!=menuDesc">
				AND menu_desc = #{menuDesc}
			</if>
			<if test="menuName!=null and ''!=menuName">
				AND menu_name = #{menuName}
			</if>
			<if test="menuAppId!=null and ''!=menuAppId">
				AND menu_app_id = #{menuAppId}
			</if>
			<if test="startTime1!=null and ''!=startTime1">
				<![CDATA[AND menu_create_time >= #{startTime1}]]>
			</if>
			<if test="endTime1!=null and ''!=endTime1">
				<![CDATA[AND menu_create_time < #{endTime1}]]>
			</if>
			<if test="menuCreatePid!=null and ''!=menuCreatePid">
				AND menu_create_pid = #{menuCreatePid}
			</if>
			<if test="menuCreatePname!=null and ''!=menuCreatePname">
				AND menu_create_pname = #{menuCreatePname}
			</if>
			<if test="startTime2!=null and ''!=startTime2">
				<![CDATA[AND menu_modify_time >= #{startTime2}]]>
			</if>
			<if test="endTime2!=null and ''!=endTime2">
				<![CDATA[AND menu_modify_time < #{endTime2}]]>
			</if>
			<if test="menuModifyPid!=null and ''!=menuModifyPid">
				AND menu_modify_pid = #{menuModifyPid}
			</if>
			<if test="menuModifyPname!=null and ''!=menuModifyPname">
				AND menu_modify_pname = #{menuModifyPname}
			</if>
			<if test="menuCode!=null and ''!=menuCode">
				AND menu_code = #{menuCode}
			</if>
			<if test="menuStatus!=null">
				AND menu_status = #{menuStatus}
			</if>
		</trim>
	</sql>

	<sql id="_primaryKey_condition">
		WHERE id = #{id}
	</sql>

	<sql id="_order">
		<choose>
			<when test="sortByParamNames != null and sortByParamTypes !=null and sortByParamNames !='' and sortByParamTypes !=''" >
				ORDER BY ${sortByParamNames} ${sortByParamTypes}
			</when>
			<when test="sortByParamNames !=null and sortByParamNames !=''">
				ORDER BY ${sortByParamNames}
			</when>
			<otherwise>
				ORDER BY ${sortByParamTypes}
			</otherwise>
		</choose>
	</sql>

	<insert id="addPrivilegeMenu" parameterType="PrivilegeMenuQDto" keyProperty="id">
		INSERT INTO <include refid="_table"/>
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="id!=null">
				id,
			</if>
			<if test="menuDesc!=null and ''!=menuDesc">
				menu_desc,
			</if>
			<if test="menuName!=null and ''!=menuName">
				menu_name,
			</if>
			<if test="menuAppId!=null and ''!=menuAppId">
				menu_app_id,
			</if>
			<if test="menuCreateTime!=null and ''!=menuCreateTime">
				menu_create_time,
			</if>
			<if test="menuCreatePid!=null and ''!=menuCreatePid">
				menu_create_pid,
			</if>
			<if test="menuCreatePname!=null and ''!=menuCreatePname">
				menu_create_pname,
			</if>
			<if test="menuModifyTime!=null and ''!=menuModifyTime">
				menu_modify_time,
			</if>
			<if test="menuModifyPid!=null and ''!=menuModifyPid">
				menu_modify_pid,
			</if>
			<if test="menuModifyPname!=null and ''!=menuModifyPname">
				menu_modify_pname,
			</if>
			<if test="menuCode!=null and ''!=menuCode">
				menu_code,
			</if>
			<if test="menuStatus!=null">
				menu_status,
			</if>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
			<if test="id!=null">
				#{id},
			</if>
			<if test="menuDesc!=null and ''!=menuDesc">
				#{menuDesc},
			</if>
			<if test="menuName!=null and ''!=menuName">
				#{menuName},
			</if>
			<if test="menuAppId!=null and ''!=menuAppId">
				#{menuAppId},
			</if>
			<if test="menuCreateTime!=null and ''!=menuCreateTime">
				#{menuCreateTime},
			</if>
			<if test="menuCreatePid!=null and ''!=menuCreatePid">
				#{menuCreatePid},
			</if>
			<if test="menuCreatePname!=null and ''!=menuCreatePname">
				#{menuCreatePname},
			</if>
			<if test="menuModifyTime!=null and ''!=menuModifyTime">
				#{menuModifyTime},
			</if>
			<if test="menuModifyPid!=null and ''!=menuModifyPid">
				#{menuModifyPid},
			</if>
			<if test="menuModifyPname!=null and ''!=menuModifyPname">
				#{menuModifyPname},
			</if>
			<if test="menuCode!=null and ''!=menuCode">
				#{menuCode},
			</if>
			<if test="menuStatus!=null">
				#{menuStatus},
			</if>
		</trim>
	</insert>

	<update id="updatePrivilegeMenu" parameterType="PrivilegeMenuQDto">
		UPDATE <include refid="_table"/>
		<set>
			<if test="menuDesc!=null and ''!=menuDesc">
				menu_desc = #{menuDesc},
			</if>
			<if test="menuName!=null and ''!=menuName">
				menu_name = #{menuName},
			</if>
			<if test="menuAppId!=null and ''!=menuAppId">
				menu_app_id = #{menuAppId},
			</if>
			<if test="menuCreateTime!=null and ''!=menuCreateTime">
				menu_create_time = #{menuCreateTime},
			</if>
			<if test="menuCreatePid!=null and ''!=menuCreatePid">
				menu_create_pid = #{menuCreatePid},
			</if>
			<if test="menuCreatePname!=null and ''!=menuCreatePname">
				menu_create_pname = #{menuCreatePname},
			</if>
			<if test="menuModifyTime!=null and ''!=menuModifyTime">
				menu_modify_time = #{menuModifyTime},
			</if>
			<if test="menuModifyPid!=null and ''!=menuModifyPid">
				menu_modify_pid = #{menuModifyPid},
			</if>
			<if test="menuModifyPname!=null and ''!=menuModifyPname">
				menu_modify_pname = #{menuModifyPname},
			</if>
			<if test="menuCode!=null and ''!=menuCode">
				menu_code = #{menuCode},
			</if>
			<if test="menuStatus!=null">
				menu_status = #{menuStatus},
			</if>
		</set>
		<include refid="_primaryKey_condition"/>
	</update>

	<select id="findDetailPrivilegeMenu" resultMap="privilegeMenuMapper" parameterType="PrivilegeMenuQDto">
		SELECT
		<include refid="_fields"/>
		FROM <include refid="_table"/>
		<include refid="_primaryKey_condition"/>
	</select>

	<select id="findPrivilegeMenu" resultMap="privilegeMenuMapper" parameterType="PrivilegeMenuQDto">
		SELECT
		<include refid="_fields"/>
		FROM <include refid="_table"/>
		<include refid="_condition"/>
		<include refid="_order"/>
	</select>

	<select id="listPrivilegeMenu" resultMap="privilegeMenuMapper" parameterType="PrivilegeMenuQDto">
		SELECT
		<include refid="_fields"/>
		FROM <include refid="_table"/>
		<include refid="_condition"/>
		<include refid="_order"/>
		<if test="startRecord != null and pageSize != null and pageSize >0">
			limit #{startRecord},#{pageSize}
		</if>
		<if test="!(startRecord != null and pageSize != null and pageSize >0)">
			limit 0,15
		</if>
	</select>

	<select id="countPrivilegeMenu" resultType="java.lang.Integer" parameterType="PrivilegeMenuQDto">
		SELECT
		COUNT(*)
		FROM <include refid="_table"/>
		<include refid="_condition"/>
	</select>

	<delete id="deletePrivilegeMenu" parameterType="PrivilegeMenuQDto">
		DELETE FROM <include refid="_table"/>
		<include refid="_primaryKey_condition"/>
	</delete>

</mapper>

